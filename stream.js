// config file, you might want to exclude this file in .gitignore to keep confidential data
var config = require('./config.js');

var express = require('express');
var app = express.createServer(express.logger());

var _r = require("url").parse(config.REDIS_URL);
var redis = require("redis").createClient(_r.port, _r.hostname);
redis.auth(_r.auth.split(":")[1]);

var twitter = require('ntwitter');
var twit = new twitter({
  consumer_key: config.TWITTER_STREAM_CONSUMER_KEY,
  consumer_secret: config.TWITTER_STREAM_CONSUMER_SECRET,
  access_token_key: config.TWITTER_STREAM_ACCESS_TOKEN_KEY,
  access_token_secret: config.TWITTER_STREAM_ACCESS_TOKEN_SECRET
});

// config values
var ENV = config.ENV;

// if LIVE then track the real tweets
// if not track tweets which has any words of the cards for testing purpose
// for live, it should track tweets containing event tag and game tag - "#{{event}} #{{game}}"
// https://dev.twitter.com/docs/streaming-apis/parameters#track
if(ENV['LIVE']) var _track = '#'+ ENV['EVENT'] +' #'+ ENV['GAME'];
else var _track = ENV['CARDS'].join(','); // for populating "fake data"

if(ENV['LIVE']) var _regexp = new RegExp('#'+ ENV['CARDS'].join('|#'), 'ig');
else var _regexp = new RegExp(ENV['CARDS'].join('|'), 'ig');

function process_tweet_live(twt) {

  var twt_text = twt['text'].toLowerCase(); // lower case!
  var add_tweet = false; // a flag to decide if the tweet is added to db
  var type = false;
  var card = false;

  // execute queries in 1 request using multi()
  var queries = [];

  // if tweet is by @{{admin}}
  if(twt['user']['screen_name'] == ENV['ADMIN']) {
      add_tweet = true; // add tweet!

      // add to news feed - #{{game}} and #news
      if(twt_text.search('#news') > -1) {
        queries.push(['zadd', ['news_feed', Date.parse(twt['created_at']), twt['id_str']]]);
      }

      // add to prize feed - #{{game}} and #prize}}
      if(twt_text.search('#prize') > -1) {
        queries.push(['zadd', ['prize_feed', Date.parse(twt['created_at']), twt['id_str']]]);
      }

      // add to winner list - #{{game}} and #winner
      if(twt_text.search('#winner') > -1) {
        var winners = twt['entities']['user_mentions'];

        if(winners.length)
          queries.push(['zadd', ['winner_feed', Date.parse(twt['created_at']), winners[0]['screen_name']]]);
      }
      
  } else { // tweet is not by @{{admin}}

    // cards in the twt - must be just one - take the first one
    var cards_in_twt = twt_text.match(_regexp);

    // types in the twt - must be just one - take the first one
    var types_in_twt = twt_text.match(/#have|#need/ig);

    // tweet has #have, #need AND has any #{{card}}
    // have_feed, need_feed
    if(cards_in_twt && types_in_twt) {
      type = types_in_twt[0].replace('#', '');
      card = cards_in_twt[0].replace('#', '');

      add_tweet = true; // add tweet!

      // create card feed need/have
      var card_feed = [type +'_feed:'+ card, Date.parse(twt['created_at']), twt['id_str']]
      queries.push(['zadd', card_feed]);

      // create user feed
      var user_feed = ['user_feed:'+ twt['user']['screen_name'], Date.parse(twt['created_at']), twt['id_str']]
      queries.push(['zadd', user_feed]); 
    }
  }

  // only add tweet if the signal is true
  if(add_tweet) {
    // create tweet (for reference)
    var tweet = {
      'username': twt['user']['screen_name'], 
      'name': twt['user']['name'], 
      'text': twt['text'],
      'time': Date.parse(twt['created_at']),
      'card': card,
      'type': type,
      'location': twt['user']['location']
    }
    var _tweet = ['tweet:'+ twt['id_str'], tweet];
    queries.push(['hmset', _tweet]);

    // create user (for reference)
    var user = {
      'name': twt['user']['name'],
      'location': twt['user']['location']
    }
    var _user = ['user:'+ twt['user']['screen_name'], user];
    queries.push(['hmset', _user]);   
  } // if(add_tweet)

  // now execute all the queries in 1 request
  if(queries.length) {
    redis.multi(queries).exec(function(err, resp) {
      console.log('process_tweet_live -> type: '+ type +', card: '+ card);
      if(err) console.log(err);
      if(resp) console.log(resp);
    });  
  } // if(queries.length)
}

function process_tweet_fake(twt) {

  // assign have/need randomly
  if(Math.floor(Math.random()*2) == 0) var type = 'need';
  else var type = 'have';

  var twt_text = twt['text'].toLowerCase(); // lower case!
  var cards_in_twt = twt_text.match(_regexp);

  if(cards_in_twt) var card = cards_in_twt[0];
  //else var card = ENV['CARDS'][Math.floor(Math.random()*5)];
  else return;

  // execute queries in 1 request using multi()
  var queries = [];

  // create card feed need/have
  var card_feed = [type +'_feed:'+ card, Date.parse(twt['created_at']), twt['id_str']]
  queries.push(['zadd', card_feed]);

  // create tweet
  var tweet = {
    'username': twt['user']['screen_name'], 
    'name': twt['user']['name'], 
    'text': twt['text'],
    'time': Date.parse(twt['created_at']),
    'card': card,
    'type': type,
    'location': twt['user']['location']
  }
  var _tweet = ['tweet:'+ twt['id_str'], tweet];
  queries.push(['hmset', _tweet]);

  // create user
  var user = {
    'name': twt['user']['name'],
    'location': twt['user']['location']
  }
  var _user = ['user:'+ twt['user']['screen_name'], user];
  queries.push(['hmset', _user]);

  // create user_feed
  var user_feed = ['user_feed:'+ twt['user']['screen_name'], Date.parse(twt['created_at']), twt['id_str']]
  queries.push(['zadd', user_feed]);

  // add to news feed - #{{game}} and #news
  // add to prize feed - #{{game}} and #news
  // add to winner list - #{{game}} and #winner
  // all 3 above are created by accessing /load_sample_data

  // now execute all the queries in 1 request
  redis.multi(queries).exec(function(err, resp) {
    console.log('process_tweet_fake -> type: '+ type +', card: '+ card);
    if(err) console.log(err);
    if(resp) console.log(resp);
  });
}

app.get('/listen_to_stream', function(request, response) {
  response.send('Listening to stream... Please DON\'T refresh. It will interrupt streaming. Twitter also limits it to single process.');

  twit.stream('statuses/filter', {track: _track, stall_warning:true}, function(stream) {
    stream.on('data', function (data) {
      //console.log(data);
      //console.log(data['entities']['user_mentions']);
      //console.log(data['entities']['hashtags']);

      console.log('tweet by ('+ data['user']['screen_name'] +'): '+ data['text']);
      console.log('...');

      // pass on to process the tweet
      if(ENV['LIVE']) process_tweet_live(data);
      else process_tweet_fake(data);

    }); // stream.on
  }); // twit.stream
});

// for news feed, prize feed, winner list
app.get('/load_sample_data', function(request, response) {

  queries = [];

  var user = {
    'name': 'Appirio',
    'location': 'US'
  }
  var _user = ['user:appirio', user];
  queries.push(['hmset', _user]);

  
  /* news feed */
  var _news1 = {text: 'First announcement #game #news',
    'username': 'appirio', 'name': 'Appirio', 'time': 1345680106515, 'location': 'US'};
  queries.push(['hmset', ['tweet:news_1', _news1]]);

  var _news2 = {text: 'Second announcement #game #news',
    'username': 'appirio', 'name': 'Appirio', 'time': 1345680106515, 'location': 'US'};
  queries.push(['hmset', ['tweet:news_2', _news2]]);

  var _news3 = {text: 'Special news #game #news',
    'username': 'appirio', 'name': 'Appirio', 'time': 1345680106515, 'location': 'US'};
  queries.push(['hmset', ['tweet:news_3', _news3]]);

  queries.push(['zadd', ['news_feed', 1345680106515, 'news_1']]);
  queries.push(['zadd', ['news_feed', 1345680106515, 'news_2']]);
  queries.push(['zadd', ['news_feed', 1345680106515, 'news_3']]);

  /* prize feed */
  var _prize1 = {text: '$5k up for grabs! just collect these 5 cards #game #prize',
    'username': 'appirio', 'name': 'Appirio', 'time': 1345680106515, 'location': 'US'};
  queries.push(['hmset', ['tweet:prize_1', _prize1]]);

  var _prize2 = {text: 'Prize doubled now #game #prize',
    'username': 'appirio', 'name': 'Appirio', 'time': 1345680106515, 'location': 'US'};
  queries.push(['hmset', ['tweet:prize_2', _prize2]]);

  var _prize3 = {text: 'We have got a winner! find out who the winner is on the stage :) #game #prize',
    'username': 'appirio', 'name': 'Appirio', 'time': 1345680106515, 'location': 'US'};
  queries.push(['hmset', ['tweet:prize_3', _prize3]]);

  queries.push(['zadd', ['prize_feed', 1345680106515, 'prize_1']]);
  queries.push(['zadd', ['prize_feed', 1345680106515, 'prize_2']]);
  queries.push(['zadd', ['prize_feed', 1345680106515, 'prize_3']]);

  /* winners */
  queries.push(['zadd', ['winner_feed', 1345680106515, 'fmanjoo']]);
  queries.push(['zadd', ['winner_feed', 1345680206515, 'sjones']]);
  queries.push(['zadd', ['winner_feed', 1345680306515, 'scobleizer']]);
  queries.push(['zadd', ['winner_feed', 1345680306515, 'thedailyshow']]);

  queries.push(['hmset', ['user:fmanjoo', {'name': 'Farhad Manjoo', 'location': 'US'}]]);
  queries.push(['hmset', ['user:sjones', {'name': 'Steve Jones', 'location': 'US'}]]);
  queries.push(['hmset', ['user:scobleizer', {'name': 'Robert Scoble', 'location': 'US'}]]);
  queries.push(['hmset', ['user:thedailyshow', {'name': 'Jon Stewart', 'location': 'US'}]]);

  // exec all queiries in 1 request
  redis.multi(queries).exec(function(err, resp) {
    if(err) console.log(err);
    if(resp) console.log(resp);

    response.send('load_sample_data: done');
  });
});

/*app.get('/search', function(request, response) {
  response.send('Search!');

  twit.search('nodejs OR #node', {}, function(err, data) {
    console.log(data);
  });
});*/

app.get('/', function(request, response) {

  response.send(
    'The stream server is up and running...'
    +'<br />Access "/listen_to_stream" endpoint to activate streaming... the progress is logged into console'
    +'<br />Access "/load_sample_data" endpoint to load sample data... the progress is logged into console'
  );
});

app.listen(config.PORT, function() {
  console.log("Listening on " + config.PORT);
  console.log('Access "/listen_to_stream" endpoint to listen to Twitter live stream... the progress is logged into console');
  console.log('Access "/load_sample_data" endpoint to load sample data... the progress is logged into console');
});