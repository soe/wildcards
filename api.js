// config file
// exclude this file in .gitignore to keep confidential data
var config = require('./config.js');

/* initialization - starts */
var express = require('express');
var app = express.createServer(express.logger());

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', config.ALLOWED_DOMAINS);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

// configure ExpressJS
app.configure(function(){
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.use(express.session({ secret:'wildwildwest' }));

  app.use(allowCrossDomain);

  app.use('/demo', express.static(__dirname + '/app'));
  app.use(express.static(__dirname + '/public'));
});

var _r = require("url").parse(config.REDIS_URL);
var redis = require("redis").createClient(_r.port, _r.hostname);
redis.auth(_r.auth.split(":")[1]);

var twitter = require('ntwitter');
var OAuth= require('oauth').OAuth;

var oa = new OAuth(
	"https://api.twitter.com/oauth/request_token",
	"https://api.twitter.com/oauth/access_token",
	config.TWITTER_CONSUMER_KEY,
	config.TWITTER_CONSUMER_SECRET,
	"1.0",
	"http://wildcards.herokuapp.com/twitter/callback",
	"HMAC-SHA1"
);

var ENV = config.ENV;
/* initialization - ends */

/* routes - starts */

// just forwarding to the HTML5 app
// it can be hosted separately from CDN for high availability
app.get('/', function(req, res) {
	res.redirect('/demo/index.html');
});

// just forwarding to the HTML5 app
// it can be hosted separately from CDN for high availability
app.get('/app/', function(re, res) {
	res.redirect('/demo/index.html');
});

// user feed
app.get('/api/user/:username', function(req, res) {
	username = req.params.username;

	if(ENV['LIVE']) {
		// get all tweet ids user has first -> zrevrange user_feed:{{username}} 0 10
		// then loop through each tweet id to get tweet hash
		var query = ['user_feed:'+ req.params.username, 0, 10];
		redis.zrevrange(query, function(err, tweet_ids) {
			queries = [];
			for(i in tweet_ids) {
				queries.push(['hgetall', 'tweet:'+ tweet_ids[i]])
			}

			// execute all queries in 1 request
			redis.multi(queries).exec(function(err, tweets) {
				// output tweets in JSON
				res.send(tweets);
			}); // redis.multi
		});	// redis.zrevrange
	} else { // return fake data
		res.send([
			{username: username, name: username, card: ENV['CARDS'][0], type: 'need', time: 1345684762185},
			{username: username, name: username, card: ENV['CARDS'][1], type: 'need', time: 1345684762185},
			{username: username, name: username, card: ENV['CARDS'][2], type: 'have', time: 1345684762185},
			{username: username, name: username, card: ENV['CARDS'][3], type: 'need', time: 1345684762185},
			{username: username, name: username, card: ENV['CARDS'][4], type: 'need', time: 1345684762185}
		]);
	}

});

// card feed have/need
app.get('/api/:type/:card', function(req, res) {
	// get all tweet ids first -> zrevrange {{type}}_feed:{{card}} 0 20
	// then loop through each tweet id to get tweet hash
	var query = [req.params.type +'_feed:'+ req.params.card, 0, 20];
	redis.zrevrange(query, function(err, tweet_ids) {
		queries = [];
		for(i in tweet_ids) {
			queries.push(['hgetall', 'tweet:'+ tweet_ids[i]])
		}

		// execute all queries in 1 request
		redis.multi(queries).exec(function(err, tweets) {
			// output tweets in JSON
			res.send(tweets);
		}); // redis.multi
	});	// redis.zrevrange
});

// winner list
app.get('/api/winners', function(req, res) {

	// get all usernames in winner feed -> zrevrange winner_feed 0 -1
	// then loop through each username to get user hash
	var query = ['winner_feed', 'by', 'score', 'get', '#', 'get', 'user:*->name', 'get', 'user:*->location'];

	redis.sort(query, function(err, winners) {

		// built the list into list of json objects
		_winners = [];
		while(winners.length) { 
			_winners.push({username: winners[0], name: winners[1], location: winners[2]}); 
			winners.splice(0, 3); // cut out the first 3 which have been procesed
		}

		res.send(_winners);
		console.log(_winners);
	});
});

// prize feed
app.get('/api/prizes', function(req, res) {
	// get all tweet ids first -> zrevrange prize_feed:{{card}} 0 20
	// then loop through each tweet id to get tweet hash
	var query = ['prize_feed', 0, 20];
	redis.zrevrange(query, function(err, tweet_ids) {
		queries = [];
		for(i in tweet_ids) {
			queries.push(['hgetall', 'tweet:'+ tweet_ids[i]])
		}

		// execute all queries in 1 request
		redis.multi(queries).exec(function(err, tweets) {
			// output tweets in JSON
			res.send(tweets);
		}); // redis.multi
	});	// redis.zrevrange
});

// news feed
app.get('/api/news', function(req, res) {
	// get all tweet ids first -> zrevrange news_feed:{{tweet_id}} 0 20
	// then loop through each tweet id to get tweet hash
	var query = ['news_feed', 0, 20];
	redis.zrevrange(query, function(err, tweet_ids) {
		queries = [];
		for(i in tweet_ids) {
			queries.push(['hgetall', 'tweet:'+ tweet_ids[i]])
		}

		// execute all queries in 1 request
		redis.multi(queries).exec(function(err, tweets) {
			// output tweets in JSON
			res.send(tweets);
		}); // redis.multi
	});	// redis.zrevrange
});

// post the tweet for user
app.post('/twitter/post', function(req, res) {
	//console.log(req.param('token'));
	//console.log(req.param('secret'));

	var twit = new twitter({
	  consumer_key: config.TWITTER_CONSUMER_KEY,
	  consumer_secret: config.TWITTER_CONSUMER_SECRET,
	  access_token_key: req.param('token'),
	  access_token_secret: req.param('secret')
	});

	twit.verifyCredentials(function (err, data) {
    console.log(data);
  }).updateStatus(req.param('tweet'),
    function (err, data) {
      if(err) {
      	console.log(err);
      	res.send({ status_code: err.statusCode, err: err })
      }

      if(data) {
      	console.log(data);
      	res.send({ status_code: 201, data: data });
      }
    }
  );
});

// follow a user
app.post('/twitter/follow', function(req, res){

});

// get friendships - check bi-directional follow
app.post('/twitter/friendships', function(req, res){

});

// redirect user to the authorization page
app.get('/twitter/connect', function(req, res){
	oa.getOAuthRequestToken(function(error, oauth_token, oauth_token_secret, results){
		if (error) {
			console.log(error);
			res.send('Sorry, error!')
		}
		else {
			req.session.oauth = {};
			req.session.oauth.token = oauth_token;
			req.session.oauth.token_secret = oauth_token_secret;

			// store where to redirect after callback
			req.session.oauth.return_to = req.query['return_to'];
			console.log(req.query['return_to']);

			res.redirect('https://twitter.com/oauth/authenticate?oauth_token='+ oauth_token)
	}
	});
});

// receive oauth callback from twitter and redirect back to the HTML5 client
app.get('/twitter/callback', function(req, res, next){
	if (req.session.oauth) {
		req.session.oauth.verifier = req.query.oauth_verifier;
		var oauth = req.session.oauth;

		oa.getOAuthAccessToken(oauth.token,oauth.token_secret,oauth.verifier, 
		function(error, oauth_access_token, oauth_access_token_secret, results){
			if (error){
				console.log(error);
				res.send('Sorry, error!');
			} else {
				req.session.oauth.access_token = oauth_access_token;
				req.session.oauth.access_token_secret = oauth_access_token_secret;
				
				console.log(results);
				console.log(oauth_access_token);
				console.log(oauth_access_token_secret);
				console.log(req.session.oauth.return_to);

				// redirect back to the app
				var return_to = ['', ''];
				if(req.session.oauth.return_to) return_to = req.session.oauth.return_to.split('?');

				res.redirect(ENV['APP'] +'#'+ return_to[0] 
					+'?authed=1&_username='+ escape(results.screen_name)
					+'&token='+ escape(oauth_access_token)
					+'&secret='+ escape(oauth_access_token_secret)
					+'&'+ return_to[1]
				);
			}
		}
		);
	} else {
		console.log(req.session.oauth);
		next(new Error('You are not supposed to be here.'));
	}
});
/* routes - ends */

app.listen(config.PORT, function() {
  console.log("Listening on " + config.PORT);
});