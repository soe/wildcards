/*
	controller - process page request/change
*/

// listen to changePage requests - like the central controller
// and add in some logics: 
// to detect if page requested is valid
// to dynamically create pages if needed
// update the page with params from url
$(document).bind( 'pagebeforechange', function(e, data) {
	if(typeof data.toPage === 'string') {
		page_id = data.toPage.split('#')[1];

		if(page_id) {
			// check page_id if it is valid, then create it
			if(check_page(page_id)) create_page(page_id);


			// url params - passed on from jquery.mobile.params plugin
			var params = {};
			if(data.options.pageData) params = data.options.pageData;

			// card page
			if(data.toPage.indexOf('#card') > -1) {
				if(params['hot'] == 1) { // hot! so show users who need this

					var hashes = page_id.split('-');

					get_find_feed({card: hashes[1], type: 'need'}, function(feed) {
						var html = Mustache.to_html($('#users-inline-template').html(), feed);

						$('#'+ page_id +' .needy-users').prepend(html);
						$('#'+ page_id +' .hot').show();
					});
				}
			}

			// user page - fill up the params into page
			if(data.toPage.indexOf('#user') > -1) {
				$('#user .user-header .username').text('@'+ unescape(params['username']));
				$('#user .user-header .name').text(unescape(params['name']));
				$('#user .user-header .location').text(unescape(params['location']));	
				$('#user .user-header .user-icon')
					.css('background-image', 'url(http://api.twitter.com/1/users/profile_image?size=bigger&screen_name='+ params['username'] +')');
				
				get_user_feed({username: params['username'], type: params['type']}, function(resp) {
					var html = Mustache.to_html($('#tweets-template').html(), resp);
					$('#user ul.twts').html(html).listview('refresh');

					// hide need/have from card_feed depending on page type
					$('#user li.'+ params['type']).hide();
				});	

			} // user page

			// offer page - fill up the params into page
			else if(data.toPage.indexOf('#offer') > -1) {
				$('#offer .user-header .username').text('@'+ unescape(params['username']));
				$('#offer .user-header .name').text(unescape(params['name']));
				$('#offer .user-header .location').text(unescape(params['location']));
				$('#offer .user-header .user-icon')
					.css('background-image', 'url(http://api.twitter.com/1/users/profile_image?size=bigger&screen_name='+ params['username'] +')');	

				// compose message
				$('#offer .twt-box').val(create_offer_msg(
					'@'+ params['username'],
					params['type'],
					params['card'],
					params['user_card']
				));
			} // offer page

			// news page
			if(data.toPage.indexOf('#news') > -1) {
				get_news_feed({}, function(feed) {
					var html = Mustache.to_html($('#tweets-template').html(), feed);
					$('#news ul.twts').html(html).listview('refresh');

					$('#news li.twt .detail').hide();
					$('#news li.twt .text').show();
				});
			}

			// prizes page
			else if(data.toPage.indexOf('#prizes') > -1) {
				get_prizes_feed({}, function(feed) {
					var html = Mustache.to_html($('#tweets-template').html(), feed);
					$('#prizes ul.twts').html(html).listview('refresh');

					$('#prizes li.twt .detail').hide();
					$('#prizes li.twt .text').show();
				});

				get_winners_feed({}, function(feed) {
					var html = Mustache.to_html($('#users-inline-template').html(), feed);
					$('#prizes #winners-here').html(html);

					$('#winners-count').text(feed['tweets'].length);
				});
			}

			// winners page
			else if(data.toPage.indexOf('#winners') > -1) {
				get_winners_feed({}, function(feed) {
					var html = Mustache.to_html($('#users-template').html(), feed);
					$('#winners ul.twts').html(html).listview('refresh');
				});
			}

			// save access token
			if(params['authed'] && params['token'] && params['secret']) {
				// save auth token and secret
				var k_v = { 
					key: 'user', 
					value: {token: params['token'], secret: params['secret'], username: params['_username']}
				};

				// todo - move to game.data.js - somehow!
				$DB.save(k_v, function(user) {
					console.log(user);

					// on tell/offer page
					var page_id_real = page_id.split('?')[0];
					if(page_id_real.indexOf('tell') == 0 || page_id_real.indexOf('offer') == 0 ) {

						// show authorization success
						$('#'+ page_id_real +' .ui-msg.success.authed').fadeIn().delay(1800).fadeOut('slow');

						// post tweet after some delay
						window.setTimeout(function() {
							$('#'+ page_id_real +' .post-tweet').click();
						}, 300);
						
					}
				}); // $DB.save
			}
		} // if(page_id)
	}
});

// check page if it is valid or needs to be created dynamically
function check_page(page_id) {
	hashes = page_id.split('-');
	hashes[0] = hashes[0].split('?')[0];

	cards = ENV['CARDS'];
	types = ['have', 'need'];	

	if(hashes[0] == 'card') {
		if(cards.indexOf(hashes[1]) > -1) return true;
	} else if(hashes[0] == 'tell') {
		if(cards.indexOf(hashes[1]) > -1) return true;
	} else if(hashes[0] == 'find') {
		if(cards.indexOf(hashes[1]) > -1 && types.indexOf(hashes[2]) > -1) return true;
	} else if(['user', 'info', 'auth'].indexOf(hashes[0]) > -1) {
		return true;
	}

	console.log('check_page failed...');
	return false;
}