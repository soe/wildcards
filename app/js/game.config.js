// GAME PARAMETERS
// the following is also mirror of main config.js file
// it is copied here for faster reference
var ENV = {
  'ADMIN': '_wildcards',
  'SPONSOR': 'Appirio',
  'GAME': 'WildCards',
  'EVENT': 'Cloud9',
  'BITLY': 'bit.ly/_wildcards',
  'CARDS': ['passport', 'postcard', 'suitcase', 'scooter', 'map'],
  'SERVER': 'http://wildcards.herokuapp.com/',
  //'SERVER': 'http://localhost:4567/',
  'APP': 'http://wildcards.herokuapp.com/demo/index.html',
  'LIVE': false
};