/*
	This file has view helpers and view listeners
	view helpers are nice functions to help to some actions
	view listeners bind to user's click/tap and do some actions
*/


/*
	view helpers !!!
	view helpers are nice function to help to some actions
*/
// pretty month names
var MONTHS = {
	0: 'Jan', 1: 'Feb', 2: 'Mar', 3: 'Apr', 4: 'May', 5: 'Jun',
	6: 'Jul', 7: 'Aug', 8: 'Sep', 9: 'Oct', 10: 'Nov', 11: 'Dec'
};

// prettify timestamp from Epoh seconds to something like Twitter
function prettifytimestamp() {
	then = parseInt(this.time);
	now = new Date().getTime();

	diff = (now - then) / 1000;

	if(diff < 60) return diff + ' s'; 
	else if(diff < 3600) return Math.floor(diff / 60) + ' m'; 
	else if(diff < 86400) return Math.floor(diff / 3600) + ' h';
	else if(diff < 604800) return Math.floor(diff / 86400) + ' d';
	else {
		var then_date = new Date(then);
		return then_date.getDate() +' '+ MONTHS[then_date.getMonth()];
	}
}

// create page dynamically
// templating system used is mustache.js
function create_page(page_id) {

	console.log('Creating page: #'+ page_id);
	hashes = page_id.split('-');
	hashes[0] = hashes[0].split('?')[0];
	page_id = page_id.split('?')[0];

	data = ENV;
	data['card'] = hashes[1];
	data['type'] = hashes[2];

	// create page if it is not there yet
	if($('#'+ page_id).length == 0) {
		var page = Mustache.to_html($('#'+ hashes[0] +'-page-template').html(), data);
		$('body').append(page);

		if(hashes[0] == 'find') {
			get_find_feed({card: hashes[1], type: hashes[2]}, function(feed) {

				var html = Mustache.to_html($('#tweets-template').html(), feed);
				$('#'+ page_id +' ul.twts').prepend(html).listview('refresh');	
			});
			
		}
	} else {
		console.log('Already exist: #'+ page_id);
	}
}

// create offer message
function create_offer_msg(username, type, card, user_card) {

	// message template
	msg = '{{username}} Playing #'+ ENV['SPONSOR'] +' #'+ ENV['GAME'] +' at #'+ ENV['EVENT'] +'. Will #trade my #{{my_card}} for #{{your_card}}. Join the game '+ ENV['BITLY'];

	// order cards based on have/need
	if(type == 'have') {
		my_card = user_card;
		your_card = card;
	} else if(type == 'need') {
		my_card = card;
		your_card = user_card;
	}

	return msg.replace('{{username}}', username).replace('{{your_card}}', your_card).replace('{{my_card}}', my_card);
}

// authorization required dialog
function show_auth_dialog() {
	$.mobile.showPageLoadingMsg('b', 'Authorization required...', true);

	window.setTimeout(function(){
		// redirect for authorization - twitter/connect
		window.location.replace(ENV['SERVER'] + 'twitter/connect?return_to='+ escape(window.location.hash.split('#')[1]));
	}, 300);
}

/*
	view listeners !!!
	view listeners bind to user's click/tap and do some actions
*/

/* on card page - when a .needy-users .user is clicked */
// show find-{{card}}-need page
$('body').on('tap', '.needy-users .user', function() {
	hashes = window.location.hash.split('-');

	page = '#find-'+ hashes[1].split('?')[0] + '-need';

	$.mobile.changePage(page, {transition: 'slide'});
});

/* on prizes page - when a .winners .user is clicked */
// show winners page
$('body').on('tap', '.winners .user', function() {
	$.mobile.changePage('#winners', {transition: 'slide'});
});

/* on find page - tweet is clicked */
// show user page
$('body').on('tap', '.find-page li.twt', function() {
	hashes = window.location.hash.split('-');

	page = '#user?' +'username='+ escape($(this).attr('username'))
		+'&name='+ escape($(this).attr('name'))
		+'&card='+ escape(hashes[1])
		+'&type='+ escape(hashes[2])
		+'&location='+ escape($(this).attr('location'));

	$.mobile.changePage(page, {transition: 'slide'});
});

/* on user page  - tweet is clicked */
// show offer page
$('body').on('tap', '#user li.twt', function() {

	page = window.location.hash.replace('#user?', '#offer?');

	$.mobile.changePage(page +'&user_card='+ $(this).attr('card'), { transition: 'slide'});
});

/* on tell page - tell have/need is clicked */
// compose the message for the user to tweet
$('body').on('tap', '.tell-have-need a.have', function() {
	$twt_box = $(this).parents('.tell-page').find('.twt-box');

	$twt_box.val($twt_box.val().replace('#need', '#have'));
});

/* on tell page - tell have/need is clicked */
$('body').on('tap', '.tell-have-need a.need', function() {

	$twt_box = $(this).parents('.tell-page').find('.twt-box');

	$twt_box.val($twt_box.val().replace('#have', '#need'));
});

/* tweet offer/have/need is clicked */
$('body').on('tap', '.post-tweet', function(e) {
	$this = $(this);
	user = {};

	// show loading message
	$.mobile.showPageLoadingMsg('b', 'Posting...', true);

	var data = {'tweet': $this.siblings('.twt-box').val().substr(0,140)};

	/* post to twitter */
	do_twitter_post({
		data: data,
		auth_pls: show_auth_dialog,
		success: function(resp) {
			$.mobile.hidePageLoadingMsg(); 

			console.log(resp);

			if(resp.status_code == 201) {
				// go to #home page and show that tweet has been posted
				$.mobile.changePage('#home');
				$('#twt-alert').slideDown(500).delay(3000).slideUp(300);

			} else if(resp.status_code == 401) {
				show_auth_dialog();
			} else {
				// show error
				$this.siblings('.ui-msg.error').fadeIn().delay(1200).fadeOut('slow');
			}
		},
		error: function(XHR, ajaxSettings, thrownError) { // error handler
			$.mobile.hidePageLoadingMsg(); 

			// show error
			$this.siblings('.ui-msg.error').fadeIn().delay(1200).fadeOut('slow');
		}
	}); // do_twitter_post

	e.preventDefault();
	e.stopPropagation();
});

// this is to keep the user on same page if the page is refereshed
$(window).ready(function() {
	if(window.location.hash) $.mobile.changePage(window.location.hash);
});