## Deployment

The deployment process assumes that you already have git and heroku installed. If not you can refer to: https://devcenter.heroku.com/articles/heroku-command and https://help.github.com/articles/set-up-git 

First, get the project source:

> git clone https://soe@bitbucket.org/soe/wildcards.git

then get into the downloaded project folder:

> cd wildcards

once you are in the folder, create a heroku project:

> heroku create project_name

now push the project source to heroku and deploy

> git push heroku master

Then the project should be up and running at: http://project_name.herokuapp.com

The above process get _the HTML5 client app_ (app/*) and _the API server_ (api.js) running. The Procfile is configured to tell heroku to run api.js as a web process.

The HTML5 client app is just contained in the “app/folder” and is totally static. That means, it can be hosted separately from the API server. For high availability and responsiveness, it is recommeded to host it on a CDN.

The API server responds in JSON format and also acts as a buffer for Twitter authorization and proxy.

There is one more part to the project, _the stream server_ (stream.js) which listens to Twitter live stream for relevant tweets then process these tweets into Redis database. Additionally, it can live push new tweets to the HTML5 client app via Pusher/Socket.io.

So the stream server is key to feeding data into the database, so we need to run it. It can be run on heroku as a background process but I am not very sure about how long a heroku background process can run before it is terminated. I recommend running it on a dedicated machine over the duration of the event.

To run the streaming server, first make sure that we have all the required libraries.

So inside the project folder, install required packages via NPM:

> npm install

then we are ready to run it

> node stream.js

Then the stream server is up. It has 2 endpoints: 
“/load_sample_data” -  would populate synthetic data for testing purpose
“/listen_to_stream” - would listen into Twitter live stream and process relevant tweets into database

That is all to the deployment process. But there is one more: the configuration

Look into “config.js” in the source’s root. There you can update any necessary info. The file has lots of comments and the variable names are self-explanatory.

__Regarding Twitter__, you can sign up for Twitter consumer key and secret here: https://dev.twitter.com/apps - you will need 2 sets of Twitter ocnsumer key and secret. One for the API server and another for the stream server. For the stream server, you also need access token and access token secret which can be generated using OAuth tool on your Twitter application page: https://dev.twitter.com/apps/<your app id>/show

__Regarding Redis__, you can sign up for a free account here: http://redistogo.com. For production use, we can discuss to determine what plan we need.

__Regarding game parameters__, you can also define them in the config.js file.

	{
	  	'ADMIN': '_wildcards',
	  	'SPONSOR': 'Appirio',
	  	'GAME': 'WildCards',
	  	'EVENT': 'Cloud9',
	  	'BITLY': 'bit.ly/_wildcards',
	  	'CARDS': ['passport', 'postcard', 'suitcase', 'scooter', 'map'],
	  	'SERVER': 'http://wildcards.herokuapp.com/',
  		'APP': 'http://wildcards.herokuapp.com/demo/index.html',
  		'LIVE': false
	}


The game parameters of “config.js” need to be copied into “/app/js/game.config.js” file. It sounds awful for the double work. But it is an intended design, so that the HTML5 client can run without dependency on the server for simple config parameters. So whenever, you make changes for the game parameters in “config.js”, update those too in “/app/js/game.config.js”.