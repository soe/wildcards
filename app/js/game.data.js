/*
	data handler
	get respective data from API via or localstorage via Lawnchair
	-- determine if the data stored in localstorage (if any) is old/fresh
	-- if old then make the API call
*/

// todo determine if some view related function should be here
// maybe move to controller

// initial Lawnchair DB instance, DB name is 'wildcards_db'
var $DB = new Lawnchair('wildcards_db', function(e) { console.log('storage ready'); });

// API call - get_find_feed - api/{type}/{card}
function get_find_feed(params, callback) {

	var url = ENV['SERVER'] +'api/'+ params['type'] +'/'+ params['card'];

	$.getJSON(url, function(tweets) {

		var resp = {}
		resp['tweets'] = tweets;
		resp.prettifytimestamp = prettifytimestamp;

		callback(resp);
	});
}

// API call - get_user_feed - api/user/{username}
function get_user_feed(params, callback) {
	console.log('Getting user feed: #'+ page_id);

	var url = ENV['SERVER'] +'api/user/'+ params['username'];
	$.getJSON(url, function(tweets) {

		var resp = {}
		resp['tweets'] = tweets;
		resp.prettifytimestamp = prettifytimestamp;

		callback(resp);
	});
}

// API call - get_news_feed - api/news
function get_news_feed(params, callback) {
	console.log('Getting news feed');

	var url = ENV['SERVER'] +'api/news';
	$.getJSON(url, function(tweets) {

		var resp = {}
		resp['tweets'] = tweets;
		resp.prettifytimestamp = prettifytimestamp;

		callback(resp);
	});
}

// API call - get_prizes_feed - api/prizes
function get_prizes_feed(params, callback) {
	console.log('Getting prizes feed');

	var url = ENV['SERVER'] +'api/prizes';
	$.getJSON(url, function(tweets) {

		var resp = {}
		resp['tweets'] = tweets;
		resp.prettifytimestamp = prettifytimestamp;

		callback(resp);
	});
}

// API call - get_winners_feed - api/winners
function get_winners_feed(params, callback) {
	console.log('Getting winners feed');

	var url = ENV['SERVER'] +'api/winners';
	$.getJSON(url, function(tweets) {

		var resp = {}
		resp['tweets'] = tweets;
		//resp.prettifytimestamp = prettifytimestamp;

		callback(resp);
	});
}

function do_twitter_post(args) {
	$DB.get('user', function(user) {

		if(!user) {
			args.auth_pls();
			return;
		}

		if(typeof(user.value.token) != 'string') user.value.token = user.value.token[0]; 
		if(typeof(user.value.secret) != 'string') user.value.secret = user.value.secret[0]; 

		// append auth token & secret
		args.data['token'] = user.value.token;
		args.data['secret'] = user.value.secret;

		$.ajax({
			url: ENV['SERVER'] +'twitter/post',
			data: args.data,
			type: 'POST',
			dataType: 'json',
			timeout: 20000,
			success: args.success,
			error: args.error
		}); // ajax
	}); // $DB.get
}